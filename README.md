# COVID-19 en Chile

**Visualización de datos sobre el progreso de los casos confirmados de COVID-19 en Chile.** 

(probando matplotlib con datos reales y que cambia en tiempo (animación)

El primer caso registrado confirmado en Talca, Región de Maule, el 3 de marzo de 2020.

**Fuente de datos:** 
* [Ministerio de Salud Chile](https//minsal.cl)
* [Gobierno de Chile](https://www.gob.cl/coronavirus/)
* [SEREMI MINSAL Región de los Ríos](http://seremi14.redsalud.gob.cl/)


> Total Activos = Total Acumulado - Total Recuperados - Total Mortalidad

> Total Cuarentena = Total Activos + En Cuarentena (Obligatoria)

> Total Acumulado = Casos con sintomás + Casos sin sintomás


## COVID-19 en las regiones de Chile

Evolución desde 1 de marzo de 2020 | Último día registrado
---------------------------------- | ---------------------------------
![gif](https://gitlab.com/mbalinos/covid-19-en-chile/-/raw/master/COVID19CL_REG_current.gif) | ![png](https://gitlab.com/mbalinos/covid-19-en-chile/-/raw/master/barrace/COVID19CL_REG_current.png)

## COVID-19 en las comunas de la Región de los Ríos
Evolución desde 1 de marzo de 2020 | Último día registrado
---------------------------------- | ---------------------------------
![gif](https://gitlab.com/mbalinos/covid-19-en-chile/-/raw/master/COVID19CL_R14_current.gif) | ![png](https://gitlab.com/mbalinos/covid-19-en-chile/-/raw/master/barrace/COVID19CL_R14_current.png)



*Nota:*
> *Inspirado y adaptado en base del artículo:
Bar Chart Race in Python with Matplotlib by @PratapVardhan in @TDataScience https://towardsdatascience.com/bar-chart-race-in-python-with-matplotlib-8e687a5c8a41*
